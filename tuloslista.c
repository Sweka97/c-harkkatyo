/*****************************************************************************/
/* CT60A0220 C-ohjelmoinnin ja testauksen periaatteet 
 * Tekijä: Jesse Peltola
 * Päivämäärä: 07.03.2018
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto: Assari auttoi toisen structin viennin toiseen tiedostoon.
 */
/*****************************************************************************/
#include "tuloslista.h" //Sisältyy kaikki kirjastot ja nimilista.h

tulosLista *analysoiTiedot(Solmu *pAlku, tulosLista *Alku) {
	
	Solmu *ptr = pAlku;
	tulosLista *p, *pUusi;
	int summa = 0, ka = 0, i = 0, pienin = 0, maksimi = 30, max = 0, maxpituus, pituus, kokonaistila = 0, muistiprosentti;
	long int muisti; 
	char nimi[30];
	
	

	printf("Anna analysoitavalle datasetille nimi: ");
	scanf("%s", nimi);
	printf("Analysoidaan listassa olevat tiedot.\n");

	/*laskenta ja tulostus*/
	while (ptr != NULL) {	
	
		pituus = strlen(ptr->nimi);
		if (pituus < maksimi) {
			pienin = pituus;			
			maksimi = pienin;
		} else if (pituus > max) {
			maxpituus = pituus;
			max = maxpituus;
		}
		summa = summa + pituus;
		i++;
		
		kokonaistila += (sizeof(Solmu) - sizeof(char)* 30) + ((pituus+1) * sizeof(char)); //laskee paljon lista vie oikeasti muistia. Ei listan kokonaismuisti
		ptr = ptr->pNext;
	}

	ka = (summa / i);

	muisti = ceil((i * sizeof(Solmu))/1000); 
	muistiprosentti = ceil(kokonaistila * 100.0 / (i * sizeof(Solmu))); //käytetty muisti / kokonaismuisti
	
	
	printf("Datasetti Nimiä\t MinPit\t MaxPit\t KA\t Muistinkäyttö\n");
	printf("%s\t  %d\t %d\t %d\t %d\t %ld kB / %d%% \n", nimi, i, pienin, maxpituus, ka, muisti, muistiprosentti);

	/*TULOSLISTAN LUONTI*/ 
	/*Listan virheenkäsittely*/
	if ((p = (tulosLista*)malloc(sizeof(tulosLista))) == NULL) {
		perror("Muistin varaus epäonnistui.");
		exit(0);
	} else {
		/* Siirretään datatiedot listaan */
		strcpy(p->dataNimi, nimi);
		p->maara = i;
		p->pienin = pienin;
		p->maksimi = maxpituus;
		p->ka = ka;
		p->muisti = muisti;
		p->hyodyton = muistiprosentti;
		
		if(Alku == NULL) {
			Alku = p;
		} else {
			pUusi = Alku;
			while(pUusi->Next != NULL){
				pUusi = pUusi->Next;
			}
			pUusi->Next = p;
		}
	}

	return Alku;
}

void tulosta(tulosLista *Alku) {
	tulosLista *ptr = Alku;
	printf("Datasetti Nimiä\t MinPit\t MaxPit\t KA\t Muistinkäyttö\n");	
	while (ptr != NULL) {
		printf("%s\t  %d\t %d\t %d\t %d\t %ld kB / %d%% \n", ptr->dataNimi, ptr->maara, ptr->pienin, ptr->maksimi, ptr->ka, ptr->muisti, ptr->hyodyton);
		ptr = ptr->Next;

	}
}

void tallennus(tulosLista *Alku) {
	char nimi[30];
	tulosLista *ptr = Alku;
	char alku[1000];
	
	FILE *tiedosto;
	printf("Anna tallennettavan tulostiedoston nimi: ");
	scanf("%s", nimi);

	tiedosto = fopen(nimi, "a");
	/*Tiedoston virheenkäsittely*/
	if(tiedosto == NULL) {
		perror("Tiedoston avaaminen epäonnistui.\n");
		exit(0);
	} else {
		strcpy(alku, "Datasetti Nimiä\t MinPit\t MaxPit\t KA\t Muistinkäyttö\n");
		fprintf(tiedosto, "%s", alku);

		while(ptr != NULL) {
			fprintf(tiedosto, "%s\t  %d\t %d\t %d\t %d\t %ld kB / %d%% \n", ptr->dataNimi, ptr->maara, ptr->pienin, ptr->maksimi, ptr->ka, ptr->muisti, ptr->hyodyton);
			ptr= ptr->Next;
		}

		fclose(tiedosto);
	}
}

tulosLista *tyhjennys(tulosLista *Alku) {
	tulosLista *ptr = Alku;

	while(ptr != NULL) {
		Alku = ptr->Next;
		free(ptr);
		ptr = Alku;
	}
	return Alku;
}
/*****************************************************************************/
/* eof */
