/*****************************************************************************/
/* CT60A0220 C-ohjelmoinnin ja testauksen periaatteet 
 * Tekijä: Jesse Peltola
 * Päivämäärä: 07.03.2018
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 */
/*****************************************************************************/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "nimilista.h"
struct Tulos {
	char dataNimi[30];
	int maara;
	int pienin;
	int maksimi;
	int ka;
	long int muisti;
	int hyodyton;
	struct Tulos *Next;

};
typedef struct Tulos tulosLista;

tulosLista *analysoiTiedot(Solmu *pAlku, tulosLista *Alku);
void tulosta(tulosLista *Alku);
void tallennus(tulosLista *Alku);
tulosLista *tyhjennys(tulosLista *Alku);

/*****************************************************************************/
/* eof */
