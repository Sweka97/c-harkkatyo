/*****************************************************************************/
/* CT60A0220 C-ohjelmoinnin ja testauksen periaatteet 
 * Tekijä: Jesse Peltola
 * Päivämäärä: 07.03.2018
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 */
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "nimilista.h"
#include "tuloslista.h"

int main(void) {
	Solmu *pAlku = NULL;
	tulosLista *Alku = NULL;
	int valinta = 1;

	printf("Tämä ohjelma tutkii nimitietoja sisältäviä tiedostoja.\n");
	/******VALIKKO******/
	while (valinta != 0) {
		printf("1) Lue nimitiedosto\n");
		printf("2) Tulosta listassa olevat tiedot\n"); 
		printf("3) Analysoi tiedot\n");
		printf("4) Tulosta kaikki tulostiedot\n"); 
		printf("5) Tallenna kaikki tulostiedot tiedostoon\n");
		printf("6) Tyhjennä tuloslista\n"); 
		printf("0) Lopeta\n");
	
		printf("Anna valintasi: ");
		scanf("%d", &valinta);
			
		switch (valinta) {
			case 1:
				pAlku = lueTiedosto(pAlku);
				break;
			case 2:
				tulostaLista(pAlku);
				break;
			case 3: 
				Alku = analysoiTiedot(pAlku, Alku);
				break;
			case 4:
				tulosta(Alku);
				break;
			case 5:
				tallennus(Alku);
				printf("Tiedosto tallennettu.\n");
				break;
			case 6:
				Alku = tyhjennys(Alku);
				printf("Tuloslista tyhjennetty.\n");
				break;
			case 0:
				printf("Kiitos ohjelman käytöstä.\n");
				valinta = 0;
				break;
		}
	}
	return 0;
}
/*****************************************************************************/
/* eof */
