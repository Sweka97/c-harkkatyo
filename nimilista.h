/*****************************************************************************/
/* CT60A0220 C-ohjelmoinnin ja testauksen periaatteet 
 * Tekijä: Jesse Peltola
 * Päivämäärä: 07.03.2018
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 */
/*****************************************************************************/
#pragma once
struct Node {
	char nimi[30];
	int maara;
	struct Node *pNext;

};
typedef struct Node Solmu;

void tulostaLista(Solmu *pAlku);
Solmu *lueTiedosto(Solmu *pAlku);



/*****************************************************************************/
/* eof */
