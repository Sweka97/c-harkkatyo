/*****************************************************************************/
/* CT60A0220 C-ohjelmoinnin ja testauksen periaatteet 
 * Tekijä: Jesse Peltola
 * Päivämäärä: 07.03.2018
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 */
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "nimilista.h"

//Luetaan tiedosto
Solmu *lueTiedosto(Solmu *pAlku) {
	Solmu *on = pAlku, *pLoppu = NULL, *ptr;
	int i = 0;
	char tiedostonimi[30], nimet[60];
	FILE *tiedosto;

	printf("Anna luettavan tiedoston nimi: ");
	scanf("%s", tiedostonimi);
	tiedosto = fopen(tiedostonimi, "r");

	/*jos pAlku listas on jotain tietoa niin ilmoitetaan käyttäjälle ja poistetaan lista*/
	if(on != NULL) {
		printf("Poistetaan aiemmat tiedot ja luetaan uudet.\n");

		while (on != NULL) {
		pAlku = on->pNext;
		free(on);
		on = pAlku;
		}
	}
	
	/*Tiedoston virhekäsittely*/
	if (tiedosto == NULL) {
		perror("Tiedoston avaaminen epäonnistui.\n");
		exit(0);
		
	} else {
		fgets(nimet, 59, tiedosto); //Poistaa ensimmäisen rivin

		while ((fgets(nimet, 59, tiedosto)) != NULL) {
			/*Listan virheenkäsittely*/
			if ((ptr = (Solmu*)malloc(sizeof(Solmu))) == NULL) {
				perror("Muistin varaus epäonnistui.\n");
				exit(1);
			} else {
				//Lisätään listaan nimen ja määrän.
				strcpy(ptr->nimi, strtok(nimet, ";"));
				ptr->maara = atoi(strtok(NULL, "\n"));
				ptr->pNext = NULL;

				if(pAlku == NULL) {
					pAlku = ptr;
					pLoppu = ptr;
				} else {
					pLoppu->pNext = ptr;
					pLoppu = ptr;
				}
				i++;
			}
		}
		printf("Luetaan tiedosto %s.\n", tiedostonimi);
		fclose(tiedosto);
		printf("Tiedosto %s luettu, %d nimiriviä.\n", tiedostonimi, i);
	}
	return pAlku;
}

void tulostaLista(Solmu *pAlku) {
	Solmu *ptr = pAlku;	

	while (ptr != NULL) {
		printf("%s %d\n", ptr->nimi, ptr->maara);
		ptr = ptr->pNext;
	}

}
/*****************************************************************************/
/* eof */
